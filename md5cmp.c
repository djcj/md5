/*
 * Copyright (C)2013 D. R. Commander.  All Rights Reserved.
 *              2015 djcj <djcj@gmx.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * - Neither the name of the libjpeg-turbo Project nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS",
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include "./md5.h"

#define USAGE  fprintf(stderr, "USAGE: %s <file>\n", argv[0])
#define USAGE_COMPARE  fprintf(stderr, "USAGE: %s -c <correct MD5 sum> <file>\n", argv[0])

#define CHECK_FOR_MD5SUM(md5hash) \
md5sum = MD5File(md5hash, buf); \
if (!md5sum) { \
	perror("Could not obtain MD5 sum"); \
	return -1; \
}

int main(int argc, char *argv[])
{
	char *md5sum = NULL, buf[65];

	if (argc > 1) {
		if (strcmp(argv[1], "-c") == 0) {
			if (argc < 3) {
				USAGE_COMPARE;
				return -1;
			}

			if (strlen(argv[2]) != 32) {
				fprintf(stderr, "WARNING: MD5 hash size is wrong.\n");
				return -1;
			}

			CHECK_FOR_MD5SUM(argv[3]);

			if (!strcasecmp(md5sum, argv[2])) {
				fprintf(stderr, "%s: OK\n", argv[3]);
				return 0;
			} else {
				fprintf(stderr, "%s: FAILED.  Checksum is %s\n", argv[3], md5sum);
				return -1;
			}
		} else {
			if (argc < 2) {
				USAGE;
				return -1;
			}

			CHECK_FOR_MD5SUM(argv[1]);

			fprintf(stderr, "%s\n", md5sum);
			return 0;
		}
	}
	USAGE;
	USAGE_COMPARE;
	return -1;
}

