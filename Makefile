SRCS = md5.c md5cmp.c md5hl.c

OBJS = $(SRCS:%.c=%.o)

LD := g++
CC := gcc
STRIP := strip


all: md5

clean:
	rm -f md5 $(OBJS)

md5: $(OBJS)
	$(LD) $(LDFLAGS) -o $@ $^
	$(STRIP) $@

%.o: %.c
	$(CC) -c -O3 -Wall -Wextra $(CFLAGS) $(CPPFLAGS) -o $@ $<

